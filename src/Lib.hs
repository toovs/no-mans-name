{-# LANGUAGE OverloadedStrings #-}

module Lib
  ( noMansName
  ) where

import System.IO
import Data.Maybe
import Data.List
import Data.Text.ICU.Regex
import Text.JSON
import Control.Monad
import Network.HTTP
import qualified Data.Text as T

data Portmanteau = Portmanteau String deriving (Show, Eq)

noMansName :: IO ()
noMansName = nameWhat

nameWhat :: IO ()
nameWhat = do
  putStrLn "\nWhat do you want to name?\n"
  putStrLn "1. System"
  putStrLn "2. Planet"
  putStrLn "3. Fauna\n"
  choice <- getLine
  case head choice of
    '1' -> nameSystem
    '2' -> namePlanet
    '3' -> nameFauna
    _   -> nameWhat

nameSystem :: IO ()
nameSystem = do
  putStr "\nEnter the region name: "
  hFlush stdout
  region <- getLine
  putStr "Enter the spectral classification: " 
  hFlush stdout
  stellar <- getLine
  case lookup ((stellar!!0):"") systemDeities of
    Nothing   -> putStrLn "spectral classification not found\n"
    Just nums -> case lookup ((stellar!!1):"") (expand nums) of
      Nothing    -> putStrLn "spectral classification not found\n"
      Just deity -> do
        printPorts deity region
        if length stellar > 2 then do
          putStr "\n\naffixes: "
          mapM_ printAffix (drop 2 stellar)
        else
          pure ()
        putStrLn "\n"

namePlanet :: IO ()
namePlanet = do
  putStr "Enter the system name: " 
  hFlush stdout
  system <- getLine
  putStr "Enter the weather descriptor: " 
  hFlush stdout
  weather <- getLine
  putStr "Enter the sentinel activity descriptor: " 
  hFlush stdout
  sentinels <- getLine
  putStr "Enter the flora descriptor: " 
  hFlush stdout
  flora <- getLine
  putStr "Enter the fauna descriptor: " 
  hFlush stdout
  fauna <- getLine
  weatherT <- translate "hi" weather
  sentinelsT <- translate "hi" sentinels
  floraT <- translate "hi" flora
  faunaT <- translate "hi" fauna
  printPorts system weatherT
  putStr $ "\n\naffix words: " ++ sentinelsT ++ " " ++ floraT ++ " " ++ faunaT
  putStrLn "\n"

nameFauna :: IO ()
nameFauna = do
  putStr "Enter the fauna genus: " 
  hFlush stdout
  genus <- getLine
  putStr "Enter the planet name: " 
  hFlush stdout
  planet <- getLine
  putStr "Enter the fauna temperament: " 
  hFlush stdout
  temperament <- getLine
  putStr "Enter the fauna diet: " 
  hFlush stdout
  diet <- getLine
  printPorts genus planet
  putStrLn ""
  let Just temperamentD = lookup temperament faunaTDeities
      Just dietD        = lookup diet faunaDDieties
  printPorts temperamentD dietD
  putStrLn "\n"

printPorts word1 word2 = do
  html <- simpleHTTP (getRequest $ "http://www.portmanteaur.com/?words=" ++ word1 ++ "+" ++ word2) >>= getResponseBody
  regexp <- regex [] $ T.pack $ "(?:(?:</span>|results\">)) ?(\\w+)<"
  setText regexp (T.pack html)
  putStrLn ""
  putStr "portmanteaus: "
  portLoop html regexp

translate :: String -> String -> IO String
translate lang str = do
  rsp <- simpleHTTP (getRequest $ "http://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=" ++ lang ++ "&dt=rm&dt=t&q=" ++ str) >>= getResponseBody
  let json' = decode rsp :: Result JSValue
  case json' of
    Error _ -> error "unable to translate"
    Ok json -> do
      if any (== lang) ["hi"] then
        let JSArray fst' = json
            JSArray snd' = head fst'
            JSArray thd  = snd' !! 1
            JSString fth = thd  !! 2
            translation  = fromJSString fth
        in return $ head $ words translation
      else
        let JSArray fst' = json
            JSArray snd' = head fst'
            JSArray thd  = head snd'
            JSString fth = head thd
            translation  = fromJSString fth
        in return $ head $ words translation

portLoop html regexp = do
  found <- findNext regexp
  if found then
    printPort html regexp >> portLoop html regexp
  else
    pure ()

printPort html regexp = do
  startI' <- start_ regexp 1
  endI'   <- end_ regexp 1
  let startI = fromIntegral startI'
      endI   = fromIntegral endI'
      len    = endI - startI
  if len > 5 && len < 13 then
    putStr $ (take (endI - startI) (drop startI html)) ++ " "
  else
    pure ()

printAffix char = putStr $
  case char of
    'e' -> "ei "
    'f' -> "ef "
    'h' -> "ah "
    'k' -> "ak "
    'm' -> "im "
    'n' -> "in "
    'p' -> "op "
    'q' -> "oq "
    's' -> "us "
    'w' -> "uw "

expand ls = concat $ map expand1 ls
  where expand1 (ft,sd) = map (expand2 sd) (words ft)
          where expand2 sd ft' = (ft',sd)

systemDeities = [("O",[("0 1","Morrigan"),("2 3","Shiva"),("4 5","Susanoo"),("6 7","Tabhadra"),("8 9","Loki")]),("B",[("0 1","Danu"),("2 3","Buddha"),("4 5","Benten"),("6 7","Shinje"),("8 9","Odin")]),("A",[("0 1","Dagda"),("2 3","Krishna"),("4 5","Amaterasu"),("6 7","Vairocana"),("8 9","Thor")]),("F",[("0 1","Brigit"),("2 3","Vishnu"),("4 5","Izanagi"),("6 7","Begtse"),("8 9","Baldur")]),("G",[("0 1","Arawn"),("2 3","Indra"),("4 5","Tsukiyomi"),("6 7","Dharma"),("8 9","Freya")]),("K",[("0 1","Lugh"),("2 3","Yama"),("4 5","Bishamon"),("6 7","Dalai"),("8 9","Magni")]),("M",[("0 1","Tuatha"),("2 3","Brahma"),("4 5","Fuku"),("6 7","Adi"),("8 9","Hel")]),("L",[("0 1","Aine"),("2 3","Durga"),("4 5","Hoori"),("6 7","Manjusri"),("8 9","Tyr")]),("T",[("0 1","Abandinus"),("2 3","Maitreya"),("4 5","Aizen"),("6 7","Yaguru"),("8 9","Heimdall")]),("Y",[("0 1","Medb"),("2 3","Varuna"),("4 5","Amida"),("6 7","Tashi"),("8 9","Valkyries")]),("E",[("0 1","Borvo"),("2 3","Sati"),("4 5","Uzume"),("6 7","Vajrapani"),("8 9","Forseti")])]

faunaTDeities = [("active","terasu"),("ambulatory","uzume"),("aggressive","fujin"),("amenable","hachiman"),("anxious","okami"),("bold","izanagi"),("calm","izanami"),("cautious","kotachi"),("cruel","mikoto"),("dangerous","okuni"),("defensive","kane"),("distinctive","raijin"),("docile","ryujin"),("erratic","suijin"),("fearful","susanoo"),("hibernator","tenjin"),("hostile","fukaro"),("hunter","tsukuyomi"),("migratory","zuchi"),("passive","minakata"),("predator","boshi"),("prey","hikone"),("sedate","wakahiko"),("shy","ishikori"),("skittish","kukuri"),("stalking","sakuya"),("submissive","moreya"),("timid","ichiran"),("unconcerned","nobunaga"),("unintelligent","matsumi"),("unpredictable","sukuna"),("vicious","shotoku"),("violent","yoshi"),("volatile","aizen")]

faunaDDieties = [("carnivore","amida"),("meat-eater","daruma"),("vegetation","jizo"),("oxide","kannon"),("nutrients","yakushi"),("herbivore","daikoku"),("grazing","ebisu"),("scavenger","hotei"),("insect-eater","jurojin")]
